/*
  * Modernizr tests
*/

if(Modernizr.touch) {
  Modernizr.load({
      load: ['js/mobile.1.4.js']
  });
}

/*
  * https://gist.github.com/beiyuu/5958004
*/
 var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();


     /* http://stackoverflow.com/questions/15254324/jquery-orientation-with-window-resize-ooops */
// $(window).resize( function(){
// }).resize();

/*
  * End
*/

/*
  * Slide down search and sub navigation menu
*/
$(function(){
  var nb = $('#navbtn');
  var n = $('.searchpad nav');

// https://gist.github.com/padolsey/527683
//  ----------------------------------------------------------
// A short snippet for detecting versions of IE in JavaScript
// without resorting to user-agent sniffing
// ----------------------------------------------------------
// If you're not in IE (or IE version is less than 5) then:
// ie === undefined
// If you're in IE (>=5) then you can determine which version:
// ie === 7; // IE7
// Thus, to detect IE:
// if (ie) {}
// And to detect the version:
// ie === 6 // IE6
// ie > 7 // IE8, IE9 ...
// ie < 9 // Anything less than IE9
// ----------------------------------------------------------

// UPDATE: Now using Live NodeList idea from @jdalton

var ie = (function(){

  var v = 3
    , div = document.createElement( 'div' )
    , all = div.getElementsByTagName( 'i' )
  do
    div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->'
  while
    (all[0])
  return v > 4 ? v : document.documentMode


/* above taken from davidhellsing comment on above gist */
  // var undef,
  // v = 3,
  // div = document.createElement('div'),
  // all = div.getElementsByTagName('i');
  // while (
  //   div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
  //   all[0]
  // );
  // return v > 4 ? v : undef;
}());

// IE8 bug where search won't show until resize. Fix below. 11/15/13 CL
if (ie === 8){
  if(nb.is(':hidden') && n.is(':hidden') && $(window).width() > 767) {
      $('.searchpad nav').show().addClass('keep-nav-closed');
    }
  $("#toTop").addClass('invisible');
}

$(window).on('resize', function(){
// if($(this).width() < $(this).height()) {
//   alert('woohoo');
// }
  if($(this).width() < 768 && n.hasClass('keep-nav-closed')) {
    // if the nav menu and nav button are both visible,
    // then the responsive nav transitioned from open to non-responsive, then back again.
    // re-hide the nav menu and remove the hidden class
    $('.searchpad nav').hide().removeAttr('class');
  }

  if(nb.is(':hidden') && n.is(':hidden') && $(window).width() > 767) {
    // if the navigation menu and nav button are both hidden,
    // then the responsive nav is closed and the window resized larger than 560px.
    // just display the nav menu which will auto-hide at <560px width.
    $('.searchpad nav').show().addClass('keep-nav-closed');
  }
});

  $('.searchpad nav a').on('click', function(e){
    e.preventDefault(); // stop all hash(#) anchor links from loading
  });

  $('#navbtn').on('click', function(e){
    e.preventDefault();
     $(".searchpad nav").slideToggle(350); // for desktop
     $("#muh").toggleClass('expanded'); // for mobile
  });

});

var adjustLayout = function() {
    if ($(window).width() < 768) {
      $('li a[class*="icon-"]').addClass('ir');
    }
    else {
      $('li a[class*="icon-"]').removeClass('ir');
    }
};

/*
  * Scroll to Top
  * http://webdesignerwall.com/demo/scroll-to-top/scrolltotop.html
*/

$(document).ready(function(){
// hide #back-top first
    $("#back-top").hide();

    // fade in #back-top
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#back-top a').click(function () {

            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

});


$(function() {
    adjustLayout();
});

$(window).on('resize', function() {
    adjustLayout();
});



/*
  * year in footer
*/
$("#date").append("&copy; " + (new Date).getFullYear());


/*
  * Device Pixel Ratio test
  * http://www.kylejlarson.com/blog/2012/creating-retina-images-for-your-website/
  * Load retina images on devices that support hi res images
*/
$(function () {
  if (window.devicePixelRatio == 2) {
    var images = $("img.hires");
    var imageType;
    var imageName;
    var initWidth;
    var initHeight;

    // loop through the images and make them hi-res
    for(var i = 0; i < images.length; i++) {
      //init dimensions
      initWidth = $(images[i]).width();
      initHeight = $(images[i]).height();

      // create new image name
      imageType = images[i].src.substr(-4);
      imageName = images[i].src.substr(0, images[i].src.length - 4);
      imageName += "@2x" + imageType;

      //rename image
      images[i].src = imageName;

      //reset dimensions
      $(images[i]).width(initWidth);
      $(images[i]).height(initHeight);
    }
  }
});


