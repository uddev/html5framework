# UDBrand HTML5 Template Framework #
* This is a static web site template based on html5. 
* WCAG 2.0 AA Compliance
* Retina ready
* Responsive grid layout. (Is not setup to load content based on device.)
* Works on desktop (PC/Mac, Firefox, IE9-11, Safari, Chrome)
* Works on mobile (iPad, Kindle, Android 4+, iOS 6-8)

### What is this repository for? ###

* UD templating framework that includes UD branded header and footer and allows developers to add content without having to worry about it displaying correctly on mobile browsers. It is meant as a starting skeleton that you can incorporate into your language (php, .NET, etc.) to take full advantage of [RESS](http://www.lukew.com/ff/entry.asp?1392) techniques.


### Some Helpful Tips ###

[git - the simple guide](http://rogerdudler.github.io/git-guide/)
[git workflow - by Ben Mearns](http://sites.udel.edu/research-computing/2014/12/3-git-workflows-with-bitbucket)


### Contribution guidelines ###

* Fork repository and make sure to keep the naming conventions the same.
* Code review
* Other guidelines

### Who do I talk to? ###

* cleonard@udel.edu
* mearns@udel.edu

### Demos ###


* Please note that some files are referencing www.udel.edu/deptforms/.... as I'm trying to keep the core centralized for easy updating. The framework will work correctly AS IS ON COPLAND SITES ONLY.